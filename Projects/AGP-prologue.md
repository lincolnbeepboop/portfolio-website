---
layout: projectspage
projnum: 3
permalink: /AGP_prologue/
title: "Climbing Trees"
description: "Prologue to 'A Green Phlonia'"
quote: "In a time of an energy crisis, a new technology nicknamed 'the haze' is developed in the form of parasitic mechanical flowers, it efficiently generates usable energy from the stored energy in botanical life."
tools: "Photoshop, Blender and Gravity Sketch were used."
---

# Reel

<iframe width="1280" height="720"
src="https://youtube.com/embed/Q5NRAQSwTho">
</iframe>
Rendered in Eevee.

# Final shots

![](/assets/img/proj/green/wide.jpg )
Wideshot of entire hill.

\
![](/assets/img/proj/green/flowers.jpg )
Close-up of the haze flowers.

\
![](/assets/img/proj/green/reach.jpg )
The climbing tree.

From the bottom we see dead remains of the trees, a few in the process of dying, and one seemingly climbing the hill, desperately hanging on to life. The Haze has nearly taken over this entire environment.  


# Planning, sketches, and 3D

![](/assets/img/proj/green/portfolio_RoughSketches.jpg)

Rough planning, I wanted the viewer to connect with and feel empathy towards the climbing tree.

![](/assets/img/proj/green/climbing_treeVR.jpg)

The tree was sculpted quickly in VR (gravity sketch) deliberately with branches resembling outreaching human hands.

![](/assets/img/proj/green/phlonia_3d.jpg)

![](/assets/img/proj/green/portfolio_EnvRenders.jpg)

Renders and variations on lighting for the environment shot. 


## Other Worldbuilding Sketches

[<img src="/assets/img/proj/green/epicentre-building-jpg12.jpg" width="700"/>](/assets/img/proj/green/epicentre-building-jpg12.jpg "Sketches") 
\
Early sketch of the factory where the robotic flowers were developed, surrounded by hills of trees. 

