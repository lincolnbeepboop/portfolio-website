---
layout: projectspage
projnum: 2
permalink: /a-green-phlonia/
title: "A Green Phlonia"
description: "Another personal project started from a desire to explore one of my daily concept paintings more indepth."
quote: "A dystopian future where plant life is scarce, wood is more valuable than gold, and oxygen is manufactured -- a group of scientists discovers a rapid growth agent able to bring greenery back to the world. Conflict ensues when they realize the ruling empire of Phlonia would much rather keep the status quo."
tools: "Photoshop, Blender and Gravity Sketch were used."
---

![](/assets/img/proj/green/cityline1.jpg "Polluted cityline where the story takes place")

![](/assets/img/proj/green/shot5.jpg "Paintover for shot 5")

![](/assets/img/proj/green/shot6.jpg "Paintover for shot 6")

![](/assets/img/proj/green/shot8.jpg "Paintover for shot 8")

![](/assets/img/proj/green/cityline2.jpg "Variation on sniper keyframe")

[<img src="/assets/img/proj/green/male-portait-final-2.jpg" width="700"/>](/assets/img/proj/green/male-portait-final-2.jpg "Final design") \

<br>


# Planning, Sketches, Design & 3D

![](/assets/img/proj/green/comps-sniper.jpg "Initial drafts")

![](/assets/img/proj/green/sniper-shot-PORTFOLIO.jpg "Initial drafts")

Initial planning for the sniper roof-top keyframe. Created in Blender, the focus was to explore different compositions and designs in 3D, focusing on graphic lighting.

This was followed by set dressing the rooftop and rendering a few stills to paintover.

<iframe width="1280" height="720"
src="https://www.youtube.com/embed/lg4KcYVS80k">
</iframe>

I sculpted a pigeon, animated variations of the pigeon walking, pecking, and flying in a boid simulation system (Blender) in order help aid in creating dynamic shots to paint-over.


![](/assets/img/proj/green/env_making-of.jpg "City 3D")

Design of the city through rough 3D concepting of buildings, skyscrapers, modelled in 3D Coat and arranged in Blender.


## Characters


![](/assets/img/proj/green/male-mc-portfolio.jpg "Phlonia Warrior Design and Planning")

![](/assets/img/proj/green/female_sniper.jpg "Phlonia female sniper Design and Planning")


# Exploration Sketches 

[<img src="/assets/img/proj/green/phlonia_park.jpg" width="700"/>](/assets/img/proj/green/phlonia_park.jpg "Environment Sketch") \
Exploration sketch for the environment. A futuristic park devoid of natural greenery, only mechanical flowers. 

[<img src="/assets/img/proj/green/20210811.jpg" width="700"/>](/assets/img/proj/green/20210811.jpg "Conversation starter")

This quick daily concept acted as a conversation starter for the entire project, I wanted to develop a Sci-Fi world and went back to this sketch to find inspiration from old work. 

\
\
\
More coming soon. 