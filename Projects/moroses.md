---
layout: projectspage
projnum: 4
permalink: /moroses/
title: "Moroses Stealth Game"
description: "A fun indie game project with a group of close friends. A 2D story driven stealth game. I am working as the lead artist with a lot of artistic control, responsible for all concept art, character and environment design, UI design, pixel art, VFX, and animation." 
quote: 
tools:  "Art - Photoshop, substance designer, blender, aesprite. Programming - in C++ with the SDL2 library. Version control is done in Git."
---

## Characters

[<img src="/assets/img/proj/moroses/char_variations.jpg" width="1200"/>](/assets/img/proj/moroses/char_variations.jpg)

Variations on style for the main character as a child, experimented with more realistic and anime-like styles emphasizing a cute but mischevious little girl - soon to be expert thief. 


[<img src="/assets/img/proj/moroses/char_teacher-splash.jpg" width="800"/>](/assets/img/proj/moroses/char_teacher-splash.jpg)

Illustration for the 'Teacher' character, who will be an inspiration for our main character on her journey to thiefdom. 


[<img src="/assets/img/proj/moroses/teacher_outfit-concept1.jpg" width="800"/>](/assets/img/proj/moroses/teacher_outfit-concept1.jpg)

Rough concepting for outfits for the Teacher. The final illustration ended up as a mixture of these outfits.

[<img src="/assets/img/proj/moroses/the_librarian.jpg" width="800"/>](/assets/img/proj/moroses/teacher_outfit-concept1.jpg)

The librarian, a strict, noise sensitive enemy in a planned level. 

[<img src="/assets/img/proj/moroses/npcs.jpg" width="800"/>](/assets/img/proj/moroses/npcs.jpg)

Some NPCs, including the businessman spirit, a ghost located in the library.

## Environment 

[<img src="/assets/img/proj/moroses/pilot_kitchen-masterPSD.jpg" width="1200"/>](/assets/img/proj/moroses/pilot_kitchen-masterPSD.jpg)


The initial first level of the game has the main character tasked to steal a cookie from the bakery. This is to test out the look of the game and the core mechanics. 

<br>

![](/assets/img/proj/moroses/pilot_home-cutaway.jpg)
Future ideas for maps and designs include environments in differing perspectives and cutaways. This is a concept for the house for the main character. 

<br>
![](/assets/img/proj/moroses/library-examplerender.jpg)

To template creating future levels more easily, I developed a procedural interior builder Blender's geometry nodes - with replaceable assets for interior decoration. This allows for more iteration on future levels, with the ability to test the look of an environment very easily with controls for size, asset types, and distribution of assets, all with accurate lighting, along with the ability to bake and make major or minor adjustments to the procedurally generated interiors. 

<iframe width="1280" height="720"
src="https://www.youtube.com/embed/tqx015Khsuk">
</iframe>


## Dialogue / UI

![](/assets/img/proj/moroses/visual_novelmode.jpg)
Portraits are being developed for the main cast of characters as well as different expressions. Narratively, the aim is for the main characters thievery to have major consequences to the rest of the casts' lives, behaviour, and relationship to the main character.

![](/assets/img/proj/moroses/vn.jpg)
UI Design for dialogue. Other aspects of UI design for overlays, item inventory, menus, and more are being developed.


<br>

## Animation/Pixel Art

![](/assets/img/proj/moroses/mainchar_child_animation.gif)
![](/assets/img/proj/moroses/main_chair-dash.gif)
![](/assets/img/proj/moroses/mainchar_child_hook.gif)
![](/assets/img/proj/moroses/librarian_walk_8dir-sample.gif)
![](/assets/img/proj/moroses/elderly_walk.gif)
![](/assets/img/proj/moroses/LampMonster-Walk-02.gif)

