---
layout: projectspage
projnum: 1
permalink: /the_ghost_samurai/
title: "The Ghost Samurai"
description: "This personal project started from a desire to explore one of my daily concept paintings more indepth."
quote: "A rising daimyo's castle is taken over by a vengeful ghost of a samurai he once commanded."
tools: "Photoshop, substance designer, marmoset toolbag, blender were used."
---



# Final paintover - Interior
![](/assets/img/proj/shot1.jpg "Final paintover")

<br>

![](/assets/img/proj/shot2.jpg "Final paintover")

<br>

![](/assets/img/proj/shot3.jpg "Final paintover")
As the sun falls, the daimyo cautiously approaches a mysterious light in his suddenly empty castle.

<br>

## Texturing 
Textures for the interior and for the Daimyo's armour were created in substance designer, allowing for quick iterations of colour schemes. 


![](/assets/img/proj/texts_house.jpg "Textures for environment")
Textures for interior include: ceiling, floor, gate, pillar, walls, and far gate wall. 

![](/assets/img/proj/texts_samurai.jpg "Textures for samurai")
Textures for the samurai created: sword hilt, armour pattern.


## Variations

[<img src="/assets/img/proj/textvar.jpg" width="700"/>](/assets/img/proj/textvar.jpg "Interior variation 1")



[<img src="/assets/img/proj/textvar2.jpg" width="700"/>](/assets/img/proj/textvar2.jpg "Interior variation 2")



[<img src="/assets/img/proj/light1.jpg" width="700"/>](/assets/img/proj/light1.jpg "Interior variation 3")


# Final paintover - Exterior
[<img src="/assets/img/proj/castle_version2.jpg" width="900"/>](/assets/img/proj/castle_version2.jpg "Concept for castle")

<br> 

![](/assets/img/proj/temp-castle.jpg "Polished castle")

Rough concepting for exterior castle grounds - then modelled in Blender with photoshop paint over. 