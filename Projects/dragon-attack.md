---
layout: projectspage
projnum: 7
permalink: /dragon-attack/
title: "Dragon Attack"
description: "Project testing a workflow for 3D Coat to Unreal Engine 5 for quick concepting, real time lighting and fast rendering"
quote: ""
tools: "Photoshop, 3DCoat, and Unreal Engine 5 were used."
---

# Paint over 



![](/assets/img/proj/dragon-attack/DragonViewing.jpg "Dragon arrives on top of the Castle")

![](/assets/img/proj/dragon-attack/DragonFight_Final2.jpg "Fight with the dragon")

Final Paintovers in Photoshop. Medieval Knight and Castle assets from Big Medium Small.


<iframe width="1280" height="720" src="https://www.youtube.com/embed/ixYjDCrFvak" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
Real-time fly by shot in UE5 

# Sculpts
![](/assets/img/proj/dragon-attack/dragon-portfolio.jpg "Sculpt and screenshot")
Sculpt in 3D and process shot in UE5. 3D Coat to UE5 proved to be quite simple to implement in order to draw the benefits of quick sculpting, texturing in 3DCoat alongside responsive and robust real time lighting and rendering in UE5. 


<a href="https://lincolnphung.com/deceitful-lands/" >See other creatures here</a>

