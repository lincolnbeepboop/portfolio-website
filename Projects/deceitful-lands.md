---
layout: projectspage
projnum: 6
permalink: /deceitful-lands/
title: "Deceitful Lands"
description: "Personal world building project, focusing on creatures, I wanted to concept an area where for those who inhabit it, nature has selected for deception, mimicry, and underhandedness. To survive, one has to always be on the look out as things are not always as they appear."
quote: ""
tools: "Photoshop, 3DCoat, Gravity Sketch, and Blender were used."
---

# The Infected Deer

![](/assets/img/proj/creatures/shot2_paintover.jpg "The infected deer, unstealthed, roaming the foggy landscape")


[<img src="/assets/img/proj/creatures/shot9_paintover-jpg12.jpg" width="500"/>](/assets/img/proj/creatures/shot9_paintover-jpg12.jpg "Close up of the infected deer, viewpoint of a small prey")

I had the idea for this personal project from thinking about the classic fantasy "mimic" treasure chest. I thought about what the equivalent would be like in nature. That led me to think of the first creature, an easy meal in the form of an inmobile, doe-eyed deer-like creature. However, underneath is a parasite that has taken over the body and using it as bait. 

![](/assets/img/proj/creatures/Creature_Sketches.jpg "Initial brainstorming for the deer")

![](/assets/img/proj/creatures/variations.jpg "Variation for concepts of the deer")

Some variations included creatures that was more obviously dead, with spider webs formed in the antlers, and more of a sense of falling apart. Although a bit more creepy, I found it didn't work with the initial premise of being bait, however it was fun to explore. 

![](/assets/img/proj/creatures/part-variations.jpg "Variations for different parts of the creature, connection between parasite and host, parasite, and host deer")

Part variations in the creature design, connection between the parasite and the deer. 

![](/assets/img/proj/creatures/renders.jpg "Renders in 3D coat")
.
.
.

# The Guide

[<img src="/assets/img/proj/creatures/guide-paint.jpg" width="500"/>](/assets/img/proj/creatures/shot9_paintover-jpg12.jpg "Tour guide to the deceitful lands")


![](/assets/img/proj/creatures/tour-guide-sculpt.jpg "Sculpts and renders in 3D coat for the guide")

While thinking of ideas for the creatures of this world, I thought of it as a playable game, if there was a guide to help the player avoid being ensared by the various traps of the wild-life. I wanted a character that seemed unpredictable, with a mix of comfort but untrustworthiness, much like everything else in this world. Based off of the idea of a faun. 

# Other Creatures

![](/assets/img/proj/creatures/dino-paintover.jpg "An armoured creature with twin tails")

![](/assets/img/proj/creatures/dino-renders.jpg "Sculpts for the armoured creature")

![](/assets/img/proj/creatures/dino-exploration.jpg "Initial planning for the armoured creature")

A dinosaur inspired creature in the deceitful lands, on the theme of "things not as they appear" I wanted a massive creature that was capable of hiding in plain sight. The main idea here was to use its long tail as a way to defend itself but also camoflague within the environment. Exploring the world, I added traces of purple infection, similar to the deer. Perhaps the infection is the root-cause of the deceitful nature of the this environment. 
\
![](/assets/img/proj/creatures/creature-sketches.jpg "Sketches")
Various quick creature sketches.
\
\
\
More coming soon.
<a href="https://lincolnphung.com/the-weather-station/" >See other creatures here</a>

