---
layout: projectspage
projnum: 5
permalink: /the-weather-station/
title: "The Weather Station"
description: "Personal fantasy project, nomads on battle stingray raid the weather station - a technological marvel that augments the weather, which takes the form of an island in the sky"
quote: ""
tools: "Photoshop, Blender, Gravity Sketch and Substance Designer were used."
---

![](/assets/img/proj/weather/shot4-regraded.jpg "Stingray warriors, final paint over")



![](/assets/img/proj/weather/shot5-jpg12.jpg "Stingray warriors, final paint over")


![](/assets/img/proj/weather/shot6-finishing-jan9.jpg "As the sun starts to set, the raiders arrive at the Weather Station, a look-out rings the bell alerting everyone inside., final paint over")


# Planning, Sketches, Design & 3D

![](/assets/img/proj/weather/portfolio-storyboard.jpg "Storyboard for raid on the weather station")



I initially had an idea of warriors on flying stingray inspired by nomadic horse riding civilizations such as the Mongols. Storyboards were quickly sketched for a scene I had in mind. During a diplomatic meeting, these warriors would raid The Weather Station, an elf inhabited island in the sky that augments the weather through advanced cloud seeding.


![](/assets/img/proj/weather/portfolio-BW-roughs.jpg "Additional world building sketches")


From the initial storyboard, I tasked myself to think of the nomadic civilization. Heavy armoured stingrays replaced horses. The stingray riders were influenced by a mix of Mongol nomads and early pilots, they stand on a large leather saddle equipped with inserts for their boots to fasten themselves to the stingray during air manuevers. Riders control the stingrays by directing their feelers. Spearholders are fastened to the saddle.


![](/assets/img/proj/weather/portfolio-battlestingray-design.jpg "Design ideas for the stingray")


![](/assets/img/proj/weather/portfolio-characterdesign.jpg "Design ideas for the nomadic raiders")


From the design stage, a full 3D model was created for the stingrays in Blender. A few custom materials and decals were created in Substance Designer. It was a fun problem to solve how a stingray would be equipped with armour and saddle, and how everything would fit together in a realistic way.

![](/assets/img/proj/weather/portfolio-battlestingray-renders.jpg "Renders for the battle stingray")


## Environment Sketches
The Weather station drew inspiration from Rivendell from The Lord of the Rings, it also drew inspiration from 'Cloud Forest' biomes on Earth, Art Nouveau, and Rococo architecture. Nature infused with manmade creation. 

![](/assets/img/proj/weather/portfolio-environment-sketches.jpg "Initial sketch ideas for the environment")


Some self-made and customized 3D assets were made for environment shots
![](/assets/img/proj/weather/portfolio-environment-3d.jpg "3D assets created for the environment")

\
\
\
More coming soon.