---
layout: projectspage
projnum: 8
permalink: /beckett/
title: "Mailman Beckett"
description: "A fun indie game project with a group of close friends. A 3D Platformer where you play as a time-travelling Mailman named Beckett"
quote: 
tools:  "Art - Photoshop, Blender, 3D Coat. Programming - Godot Engine, GDScript, C++"
---

## Keyart

![](/assets/img/proj/beckett/keyart1-fixed.jpg "First keyart for Mailman Beckett")

A fantastical world where mail has to be delivered to remote places in space and time. 

## Characters

![](/assets/img/proj/beckett/MailmanBeckett_Sketches.jpg "Beckett initial sketches")

Sketch variations

![](/assets/img/proj/beckett/beckett-coloured.jpg "Beckett poses")

Beckett character poses

<iframe width="1280" height="720" src="https://www.youtube.com/embed/5-k01tyLRcw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Initial 3D low-poly and rigged model for Beckett with an animated idle, walk, and run.

## Environments

![](/assets/img/proj/beckett/beckett-arch-la.jpg "City centre concepts")

![](/assets/img/proj/beckett/beckett-arch-colour.jpg "City centre concepts")

Concept art for an old-timey city centre.
.
.
.
.
.
More coming soon.