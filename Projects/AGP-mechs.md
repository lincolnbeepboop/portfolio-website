---
layout: projectspage
projnum: 9
permalink: /a-green-phlonia-mech/
title: "A Green Phlonia"
description: "Another personal project started from a desire to explore one of my daily concept paintings more indepth. This set explores a rogue Phlonian mech, and other Phlonian technology, such as weaponry and vehicles."
quote: "A dystopian future where plant life is scarce, wood is more valuable than gold, and oxygen is manufactured -- a group of scientists discovers a rapid growth agent able to bring greenery back to the world. Conflict ensues when they realize the ruling empire of Phlonia would much rather keep the status quo."
tools: "Photoshop, Blender and 3D Coat were used."
---


![](/assets/img/proj/phlonia/shot1-recomposite.jpg "Rogue Phlonia Mech")

![](/assets/img/proj/phlonia/shot2-recomposite.jpg "Rogue Phlonia Mech")

![](/assets/img/proj/phlonia/shot3-recomposite.jpg "Rogue Phlonia Mech")

Shots of a rogue Phlonian mech, surprising and executing another in an abandoned warehouse. Set up in Unreal 5, environment assets from Quixel. 

# Design & 3D

![](/assets/img/proj/phlonia/gundam-portfolio.jpg "Phlonia Mech turnaround")

Designs for Phlonian mech, modelled in 3D Coat, textured in Blender.

![](/assets/img/proj/phlonia/gun-portfolio.jpg "Cannon weapon")

Designs for Phlonian cannon, a heavy, destructive weapon normally used at a far range. Modelled and textured in 3D Coat. Inspired by aircraft engines.

![](/assets/img/proj/phlonia/ship-portfolio.jpg "Reconaissance ship")

Designs for a Phlonian light aircraft, inspired by an various insects - highly maneuverable and used primarily for reconaissance.
<br>
.
<br>
.
<br>
.
<br>
.
More of the Phlonia series <a href="https://lincolnphung.com/a-green-phlonia/">here</a>