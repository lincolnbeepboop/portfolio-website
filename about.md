---
layout: about
title: About Me
permalink: /about/
---

Hello,

My name is Lincoln, a freelance illustrator, 2D/3D concept artist, and 3D generalist. I am currently looking for work in games or film/TV.  

My past experience includes being an Environmental Concept Art for an unreleased indie game for the Gavyn Bryan Company, Concept Artist for Sidearm Studios, and other freelance commission based work. In my spare time I am the lead artist for Moroses, a group of close friends creating a 2D stealth game as a personal project. I have also written articles and created videos which have won the top monetary award for Clip Studio Paint's "Tips of the Month" contest on four separate occasions (Feb 2020, April 2020, July 2020, Nov 2020). You can find those tutorials [here](https://lincolnphung.com/Tutorials/).

My background is in Physics, I have a Bachelor of Physics degree and a Bachelor of Education degree from the University of Calgary. I am currently a public school substitute teacher, teaching primarily high-school Maths and Physics, with a desire to transition to the entertainment arts industry. 

For any enquires contact me at <lincolnphung@hotmail.com>
